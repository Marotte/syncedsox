#!/bin/bash
IFS=$'\t\n'
set -beuo pipefail

"${include_ok:-false}" && return

declare note_fade_in="0.008"
declare note_fade_out="0.012"
declare log_file="./$(basename "${0}").log"
declare -r  sox_call='/usr/bin/sox -q'
declare -ga sox_audio_output=("${SSOX_AUDIO_OUTPUT:--t alsa}")
declare     sox_pipe_output='-t sox - rate 48k channels 1'
declare -r  ran_file_search_path="${SSOX_SAMPLE_DIRECTORY:-$HOME}"
declare -r  file_extensions="${SSOX_SAMPLE_FILEEXT:-(flac|mp3|wav|ogg)}"
declare -r  ran_file_minsize="${SSOX_SAMPLE_MINSIZE:-5k}"
declare -r  ran_file_maxsize="${SSOX_SAMPLE_MAXSIZE:-128M}"
declare -rg sox_output_directory='./sox'
declare -rg flac_output_directory='./flac'
declare -g  output="${sox_audio_output[*]}"

echo_u () {
    echo -e "\e[4;38m${1:-}\e[m"
}

echo_b () {
    echo -e "\e[1;38m${1:-}\e[m"
}

show_help () {
    cat<<EOF

$(echo_b "SyncedSoX")

Run SoX commands snapped on time intervals.

Usage:

    $0 sox    <mode> <tempo> <snap size> <sample selection> [<sox args … >]
    $0 loop   <mode> <tempo> <snap size> <sample selection> [<loop size>] [<note length>] [<loop count>] [<delay>] [<pause size>] [<repetition count>] [<sox args … >]
    $0 play   <mode> <tempo> <snap_size> <repetitions> <voices> <file>
    $0 list   [<object>]

$(echo_u "mode")              Mode for the command.
                              Modes for the $(echo_u sox) command:
                               - alsa
                               - …
                              Modes for the $(echo_u loop) command:
                               - zero: Play the sample from position zero.
                               - random: Play the sample from a random position.

                              Modes for the $(echo_u play) command:
                               - file: Play a score file.

                              Modes for the $(echo_u list) command:
                               - loop: List loopset currently playing.
                               - sox:  List SoX instances currently playing
                               - *:    List both of the above

$(echo_u "tempo")             BPM
$(echo_u "snap size")         Expressed in quarter note according to BPM, may be a decimal number.
$(echo_u "sample selection")  File name or directory or regex. Checked in this order.
                  If it’s not a file or directory then it’s a regex searched in the default directory (currently “${ran_file_search_path}”),
                  from files with these extensions: $(tr -d '()' <<<"${file_extensions//|/,}")
                  The search is case insensitive.
                  Files being less than ${ran_file_minsize} or more than ${ran_file_maxsize} are ignored.
                  You can set the following environment variables to change those criters:
                   - SSOX_SAMPLE_DIRECTORY
                   - SSOX_SAMPLE_FILEEXT
                   - SSOX_SAMPLE_MINSIZE
                   - SSOX_SAMPLE_MAXSIZE
                   - SSOX_AUDIO_OUTPUT
$(echo_u "loop size")         Also expressed in quarter note.
$(echo_u "note length")       The note length in proportion of the loop size, expressed in percent of the loop size.
$(echo_u "loop count")        Number of times the loop will play.
$(echo_u "delay")             Expressed in quater note. Time to wait before to start looping (ie: before snaping on the grid at the specified snap size).
                  This delay is applied before the first loop. See $(echo_u "pause size") for a delay applied between each loop.
$(echo_u "pause size")        Period in quarter note between two repetitions.
$(echo_u "repetition count")  Number of times the loop set will play.
$(echo_u "sox args")          Anything SoX can do, like filters or LADSPA plugins.
$(echo_u "voices")            How many loops to start from file content.
$(echo_u "object")            Type of object to list.

Samples are synced on an absolute timeline (Epoch).
If the requested $(echo_u "note length") is longer than the sample, the sample is padded with silence to reach the desired note length.
When using the $(echo_u "loop command"), each execution is routed to ALSA, so it must be able to handle multiple audio streams, like “dmixer”, which is the default on most systems.

EOF
    exit "${1:-0}"
}

initialization () {
    for program in "/usr/bin/sox" "/usr/bin/bc" "/usr/bin/find"; do
        [[ -x "${program}" ]] || { echo -e "\n${program} not found." >&2; exit 1; }
    done
    # Check arguments validity (TODO)
    # 1:    command: sox,loop
    # 1bis  mode: (sox: alsa) (loop: zero,random) (play: file)
    # 3:    sox,loop,play/tempo: strictly positive decimal number
    # 4     sox,loop,play/snap size: strictly positive decimal number
    # 5:    sox,loop/sample selection: existing audio file OR directory OR regex
    # 6:    loop/loop size: strictly positive decimal number
    # 7:    loop/note length: strictly positive decimal number
    # 8:    loop/loop count: strictly positive integer number
    # 9:    loop/delay: positive decimal number
    # 10:   loop/pause size: positive decimal number
    # 11:   loop/repetition count: strictly positive integer number
    # 12:   play/repetition count: strictly positive integer number
    # 13:   play/min: positive decimal number
    # 14:   play/max: strictly positive decimal number
    # 15:   play/voices: strictly positive decimal number
    # 16:   play/file: “play” filename
    declare -g loopset_id
    declare -g play_id
    declare -g  status_file="/tmp/$(basename ${0})-${BASHPID}-status"
}

now        () { echo "${EPOCHREALTIME/[,.]/}"; }
timestamp  () { echo "--comment=$(now)"; }
log        () { printf "[$(date -Is)] %s\n" "${@}" >>"${log_file}"; }
perr       () { printf "\e[0;31m%s\e[m\n" "${@}" >&2; }
s2us       () { bc "${1}*1000000"; }
us2s       () { bc "${1}/1000000"; }
ranint     () { bc "scale=0;${1:-0}+($(tr -d --complement '0-9' <  <(head -n6  /dev/urandom))%((${2:-1}-${1:-0}))+1)"; }
rr         () { tr -cd "${1:-0-9}" < /dev/urandom > >(head -c"${2:-1}") || true ; }

declare bc_scale=9
declare bc_pipe="/tmp/bc_pipe_${BASHPID}"
init_bc () {
    [[ -p "${bc_pipe}" ]] || mkfifo "${bc_pipe}"
    exec 5> >(exec /usr/bin/bc -q <(echo "scale=${bc_scale}") >"${bc_pipe}")
    exec 6<"${bc_pipe}"
}
bc () {
    echo >&5 "${1}"
    read -u 6 x
    echo "${x}"
}

make_dirs () { #1: play_id
    declare -g  output_directory_name="${SSOX_EXEC_ID:-$SRANDOM}-${command}/${1}"
    [[ -d "${sox_output_directory}/${output_directory_name}" ]] \
        || mkdir -p "${sox_output_directory}/${output_directory_name}"
    [[ -d "${flac_output_directory}/${output_directory_name}" ]] \
        || mkdir -p "${flac_output_directory}/${output_directory_name}"
}

make_files () { #1: loopset_id
    declare -g  output_file_name="${1}"
    declare -g  sox_output_file="${sox_output_directory}/${output_directory_name}/${output_file_name}.sox"
    declare -g  flac_output_file="${flac_output_directory}/${output_directory_name}/${output_file_name}.flac"
}

create_files () {
    if [[ "${SHLVL}" -gt 1 ]]; then
        make_dirs "$(now)-${play_id:-$PPID}"
    else
        make_dirs "$(now)-${play_id:-$$}"
    fi
    make_files "$(now)-${loopset_id:=$BASHPID}"
    play_id="${BASHPID}"
}

ssox () {
    local T
    local source="${1:-/home}"
    local output="${2:--t alsa}"
    T="$(LANG=C printf "%2.0f\n" "$(bc "${snap_size}*${beat_length}*1000000")")"
    exec 2>>"${log_file}"
    local sleep_time_us
    sleep_time_us="$(bc "scale=0;${T}-($(now)%${T});scale=${bc_scale}")"
    local sleep_time
    sleep_time="$(bc "${sleep_time_us}/1000000")"
    echo -e "<(${sox_call}" "\"${source}\"" "$(timestamp)" "${output/$sox_audio_output/$sox_pipe_output}" "${@:3}" "pad ${sleep_time}) \\"
    eval "${sox_call}" "\"${source}\"" "${output}" "${@:3}" "pad ${sleep_time}" || true
}

loop0 () {
    declare sample_source="${1:-/dev/null}"
    declare sample_file
    if [[ -d "${sample_source}" ]]; then
        sample_file="$(/usr/bin/find -L "${1}" -xdev -type f -size -${ran_file_maxsize} -size +${ran_file_minsize} -regextype egrep -iregex '^.*\.'"${file_extensions}"'$' -print0 | shuf -z -n1 | tr '\0' '\n')"
    elif [[ -f "${sample_source}" ]]; then
        sample_file="${1}"
    else
        sample_file="$(/usr/bin/find -P "${ran_file_search_path}" -xdev -type f -size -${ran_file_maxsize} -size +${ran_file_minsize} -regextype egrep -iregex '^.*'"${1}"'.*\.'"${file_extensions}"'$' -print0 | shuf -z -n1 |tr '\0' '\n')"
    fi
    declare    loop_length_quarter_note="${2:-1}"
    declare    note_length_prct="${3:-100}"
    declare -i count="${4:-1}"
    declare    delay_length_quarter_note="${5:-0}"
    declare    pause_length_quarter_note="${6:-0}"
    declare -i loop_count="${7:-1}"
    [[ "${note_length_prct}" -le 100 ]] || { note_length_prct=100; perr "Note can’t be longer than loop, setting length to 100%."; }
    [[ "${count}" -ge 1 ]] || { count=1; perr "Loop count must be a positive integer greater or equal to 1, setting to 1."; }
    declare sample_length="$(/usr/bin/sox --info -D "${sample_file}")"
    [[ -z "${sample_length}" ]] && { perr "No valid sample found. Giving up for “${sample_source}”"; return; }
    declare loop_length="$(bc "${beat_length}*${loop_length_quarter_note}")"
    declare delay_length="$(bc "${beat_length}*${delay_length_quarter_note}")"
    declare pause_length="$(bc "${beat_length}*${pause_length_quarter_note}")"
    declare note_length="$(bc "${loop_length}*${note_length_prct}/100")"
    declare silence_length="$(bc "${loop_length}-${note_length}")"
    declare loopset_length="$(bc "${loop_length}*${count}")"
    declare -ag loop_args=("${sample_file}" "${sox_audio_output[@]}")
    [[ "$(bc "${silence_length}<0")" -eq 1 ]] && { silence_length=0; note_length="${loop_length}"; }
    if   [[ "$(bc "${note_length}<=${sample_length}")" -eq 1 ]]; then
        loop_args+=(trim 0 ${note_length} fade ${note_fade_in} 0 ${note_fade_out} pad ${silence_length})
    else
        declare sample_pad_length
        sample_pad_length="$(bc "${note_length}-${sample_length}")"
        loop_args+=(pad ${sample_pad_length} fade ${note_fade_in} 0 ${note_fade_out} pad ${silence_length})
    fi
    loop_args+=(repeat $((count-1)))
    loop_args+=(pad 0 ${pause_length} repeat $((loop_count-1)) pad ${delay_length} ${@:8})
}

loopR () {
    declare sample_source="${1:-/dev/null}"
    declare sample_file
    if [[ -d "${sample_source}" ]]; then
        sample_file="$(/usr/bin/find -L "${1}" -xdev -type f -size -${ran_file_maxsize} -size +${ran_file_minsize} -regextype egrep -iregex '^.*\.'"${file_extensions}"'$' -print0 | shuf -z -n1 | tr '\0' '\n')"
    elif [[ -f "${sample_source}" ]]; then
        sample_file="${1}"
    else
        sample_file="$(/usr/bin/find -P "${ran_file_search_path}" -xdev -type f -size -${ran_file_maxsize} -size +${ran_file_minsize} -regextype egrep -iregex '^.*'"${1}"'.*\.'"${file_extensions}"'$' -print0 | shuf -z -n1 |tr '\0' '\n')"
    fi
    declare    loop_length_quarter_note="${2:-1}"
    declare    note_length_prct="${3:-100}"
    declare -i count="${4:-1}"
    declare    delay_length_quarter_note="${5:-0}"
    declare    pause_length_quarter_note="${6:-0}"
    declare -i loop_count="${7:-1}"
    [[ "${note_length_prct}" -le 100 ]] || { note_length_prct=100; perr "Note can’t be longer than loop, setting length to 100%."; }
    [[ "${count}" -ge 1 ]] || { count=1; perr "Loop count must be a positive integer greater or equal to 1, setting to 1."; }
    declare sample_length="$(/usr/bin/sox --info -D "${sample_file}")"
    [[ -z "${sample_length}" ]] && { perr "No valid sample found. Giving up for “${sample_source}”"; return; }
    declare loop_length="$(bc "${beat_length}*${loop_length_quarter_note}")"
    declare delay_length="$(bc "${beat_length}*${delay_length_quarter_note}")"
    declare pause_length="$(bc "${beat_length}*${pause_length_quarter_note}")"
    declare note_length="$(bc "${loop_length}*${note_length_prct}/100")"
    declare silence_length="$(bc "${loop_length}-${note_length}")"
    declare max_start_position
    declare loopset_length="$(bc "${loop_length}*${count}")"
    declare -ag loop_args=("${sample_file}" "${sox_audio_output[@]}")
    [[ "$(bc "${silence_length}<0")" -eq 1 ]] && { silence_length=0; note_length="${loop_length}"; }
    if   [[ "$(bc "${note_length}<=${sample_length}")" -eq 1 ]]; then
        max_start_position="$(bc "${sample_length}-${note_length}")"
        max_start_postion_us="$(bc "$(s2us "${max_start_position}")")"
        start_position_us="$(ranint "0" "${max_start_postion_us}")"
        start_position="$(us2s "${start_position_us}")"
        loop_args+=(trim ${start_position} ${note_length} fade ${note_fade_in} 0 ${note_fade_out} pad ${silence_length})
    elif [[ "$(bc "${note_length}>${sample_length}")" -eq 1 ]]; then
        declare sample_pad_length
        max_start_position=0; sample_pad_length="$(bc "${note_length}-${sample_length}")"
        loop_args+=(pad ${sample_pad_length} fade ${note_fade_in} 0 ${note_fade_out} pad ${silence_length})
    fi
    loop_args+=(repeat $((count-1)))
    loop_args+=(pad 0 ${pause_length} repeat $((loop_count-1)) pad ${delay_length} ${@:8})
}

play () { #1: voices, #2: file
    while IFS=$' ' read -a ssox_args; do
        tempo="${ssox_args[2]}"
        snap_size="${ssox_args[3]:-1}"
        case "${ssox_args[0]}" in
            sox) : ; ;;
            loop)
                loopset_id="${loopset_id:=$BASHPID}"
                case "${ssox_args[1]}" in
                    zero)
                        loop0 "${ssox_args[@]:4}"
                        ;;
                    random)
                        loopR "${ssox_args[@]:4}"
                        ;;
                esac
                ssox "${loop_args[@]}" &
                ;;
        esac
    done <<<$(cat "${2}" | grep -Ev '(^$|^#)' | shuf -n"${1}")
    wait -f
    echo  "${output}" >> "${sox_output_file}"
}

get_cksum () { #1: file
    :
}

get_time () { #1: file
    :
}

genflac () { #1: exec_id
    # The two cases (play, and individual loops from autoloops) are handled differently
    for file in $(ls ${sox_output_directory}/${1}-play/*/*.sox | sort -n); do
        echo $file >&2
    done
    local index=0
    locat start_time
    local sample_id
    local -A tmp_flac_files
    for file in $(ls ${sox_output_directory}/${1}-loop/*/*.sox | sort -n); do
        echo $file >&2
        # get checksum (cksum -a sysv) of the sample filename
        sample_id="$(get_cksum "${file}")"
        # generate the temporary FLAC file if it doesn’t exist
        [[ -f  "${tmp_flac_files[$sample_id]}" ]] || /usr/bin/sox -q -n -t flac "${tmp_flac_files[$sample_id]}" trim 0 1s  ## Not needed if we let sox create it when index=0
        # get timestamp in file (ie: in sox command in file)
        if [[ index -eq 0 ]]; then
            local start_time="$(get_time "${file}")"
            # change '-t alsa' (or whatever ${sox_audio_output} is) with “-t flac "${tmp_flac_files[$sample_id]}"” [TODO!]
            # exec sox file [TODO]!
            # copy tmp file to sample_id file [TODO!]
        else
            pad_length="$(bc "$(get_time "${file}")-${start_time}")"
            # change “/usr/bin/sox -q \” (or whatever is ${sox_call}) by “/usr/bin/sox -q -m -t flac "${sample_id}.flac \"”
            # change ') \\n -t alsa' (or whatever ${sox_audio_output} is) with “ pad ${pad_length}) \\n -t flac "${tmp_flac_files[$sample_id]}"” [TODO!]
            # exec sox file [TODO]!
            # copy tmp file to sample_id file [TODO!]
        fi
        let $((index++))
    done
    ## Not very pleasant, can’t be paralelized without colision probability on the tmp file
}

terminate () {
    unlink "${bc_pipe}"
    #~ cat "${status_file}" >&2
    unlink "${status_file}"
}


include_ok='true'
