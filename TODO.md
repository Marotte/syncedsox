# SyncedSoX’s TODO
- Sanitize command-line arguments
- Function to check .ssox files before evaluation
- Timestamps for loops so they could be rendered as of during the direct rendition (cuuently “drone” mode cannot be ren,dered with the same timing)
- Make a frontal script more user-friendly to call the other scripts
- Compute the total loop length to get an estimation of how long one loop is.
- Kill all loop but N ones: minusN=$(($(pgrep -A -s $$ -x "sox" | wc -l)-$N )); pgrep -A -s $$ -x "sox" | shuf -n${minusN} | xargs kill -term
- Help for the "play" mode
- Make the autoloop more useful by calling the play mode
- Make all the info printing optional
- Do not produce output file when no sample is found or sample is invalid (ie: can’t be read by SoX)

